from datetime import date
from sgmllib import SGMLParser
from hashlib import md5
from StringIO import StringIO
from zipfile import ZipFile
import csv
from common import (
    BaseBrowser,
    )


class AfterLoginParser(SGMLParser):
    def __init__(self):
        SGMLParser.__init__(self)
        self.form_name = None
        self.error_message = None
        self.begin_title = False
        self.title = []
        self.data = []

    def start_form(self, attrs):
        for attr in attrs:
            if attr[0] == 'name':
                self.form_name = attr[1]

    def start_title(self, attrs):
        self.begin_title = True

    def end_title(self):
        self.begin_title = False

    def handle_data(self, data):
        data = data.strip()
        if not data:
            return
        if self.begin_title:
            self.title.append(data)
        if self.form_name in ['ErrorActionForm', 'LoginActionForm'] and \
            not self.error_message:
            self.error_message = data
        self.data.append(data)

    # Override
    def feed(self, *args, **kwargs):
        SGMLParser.feed(self, *args, **kwargs)
        if self.form_name != 'LoginActionForm':
            return
        # Siapa tahu ada pesan kesalahan yang lebih baik
        for data in self.data:
            if data.find('Error Message') > -1:
                self.error_message = data
                return
            if data.find('en.gcm.businessexception') > -1:
                self.error_message = data
                return

    def is_error(self):
        return self.error_message


def to_float(s):
    return float(s.replace(',', ''))


class SaldoParser(SGMLParser):
    def __init__(self):
        SGMLParser.__init__(self)
        self.baris = []
        self.hasil = []
        self.catat = False
        self.data = []

    def start_td(self, attrs):
        self.data = []

    def end_td(self):
        if self.catat:
            data = ' '.join(self.data)
            self.baris.append(data.strip())
            self.data = []

    def start_tr(self, attrs):
        pass

    def end_tr(self):
        if self.baris:
            self.hasil.append(self.baris)
            self.baris = []

    def start_table(self, attrs):
        pass

    def end_table(self):
        self.catat = False

    def handle_data(self, data):
        if data == 'Balance Inquiry':
            self.catat = True
            return
        if self.catat:
            data = data.strip()
            self.data.append(data)

    def get_clean_data(self):
        r = []
        jenis = self.hasil[6][1]
        for row in self.hasil[8:]:
            if len(row) < 4:
                continue
            if row[1] == 'Total':
                continue
            rekening = row[2].split()[0]
            mata_uang = row[3]
            nominal = to_float(row[4])
            r.append((rekening, jenis, mata_uang, nominal))
        return r 


def get_jrprint(s):
    for line in s.splitlines():
        if line.find('jrprint = ') == 0:
            #jrprint = "2c9082c2473d3ae301474832349b423b";
            return line.split('=')[1].strip().strip(';').strip('"')


class MutasiParser(object):
    def feed(self, content):
        zf = StringIO()
        zf.write(content)
        z = ZipFile(zf)
        filename = z.namelist()[0]
        cf = z.open(filename)
        c = csv.reader(cf)
        has_header = True
        for row in c:
            try:
                int(row[0])
                has_header = False
            except ValueError:
                pass
            break
        cf.close()
        cf = z.open(filename)
        c = csv.reader(cf)
        urutan_di_rek = {}
        self.result = []
        first = True
        for row in c:
            if first and has_header:
                first = False
                continue
            rekening = row[0]
            tgl = row[2]
            jenis = row[4]
            nominal = row[5]
            ket = row[8]
            nominal = float(nominal.replace(',', ''))
            if jenis == 'D':
                nominal = - nominal
            d, m, y = tgl.split('/')
            y = '20' + y
            tgl = date(int(y), int(m), int(d))
            if rekening in urutan_di_rek:
                urutan_di_rek[rekening] += 1
            else:
                urutan_di_rek[rekening] = 1
            urutan = urutan_di_rek[rekening]
            self.result.append((rekening, urutan, tgl, ket, nominal)) 
        cf.close()
        z.close()

    def get_clean_data(self):
        self.result.sort()
        r = []
        for rekening, urutan, tgl, ket, nominal in self.result:
            r.append((rekening, tgl, ket, nominal, 0))
        return r


DOWNLOAD_PARAMS = {
    'action': 'downloadTrxInquiry',
    'type': 'download',
    'trxFilter': '%',
    'screen': 'search'}

ERR_BEFORE_LOGIN = 'Currently Unable'

class Browser(BaseBrowser):
    def __init__(self, username, password, group, parser, output_file=None):
        super(Browser, self).__init__('https://www.permatae-business.com',
            username, password, parser, output_file=output_file)
        self.group = group

    def login(self):
        content = self.get_content()
        q = {'action': 'loginPopupRequest'}
        content = self.get_content('/corp/common/login.do', GET_data=q)
        q = {'action': 'loginRequest'}
        content = self.get_content('/corp/common/login.do', GET_data=q)
        if str(content).find(ERR_BEFORE_LOGIN) > -1:
            self.last_error = ERR_BEFORE_LOGIN  
            return
        self.br.select_form(nr=0)
        self.br.set_all_readonly(False)
        self.br['userName'] = self.username 
        self.br['corpId'] = self.group 
        self.br['language'] = ['fr_FR']
        self.br['password'] = md5(self.password).hexdigest()
        self.br.new_control('HIDDEN', 'action', {})
        control = self.br.form.find_control('action')
        control.readonly = False
        self.br['action'] = 'login'
        resp = self.br.submit()
        content = resp.read()
        parser = AfterLoginParser()
        parser.feed(content)
        error = parser.is_error()
        if error:
            self.last_error = error
        else:
            return True

    def logout(self):
        q = {'action': 'logout'}
        self.open_url('/corp/common/login.do', GET_data=q)


SALDO_PARAMS = {
    'action': 'getReportAccountInquiryByDate',
    'accountPick': 'multiple',
    'selectby': '3',
    'hierarchy': ' ',
    'accountType': ' ',
    'accountBranch': ' ',
    'menu': 'search',
    'tabName': 'Balance Inquiry',
    'chooseGroupBy': 'Date'}

class SaldoBrowser(Browser):
    def __init__(self, username, password, group, output_file=None):
        Browser.__init__(self, username, password, group, SaldoParser,
                         output_file)

    def browse(self):
        q = {'action': 'menuRequest'}
        self.open_url('/corp/common/login.do', GET_data=q) 
        q = {'action': 'balanceRequest',
             'menuCode': 'MNU_GCME_040100'}
        self.open_url('/corp/front/balanceinquiry.do', GET_data=q)
        tgl = date.today()
        p = dict(SALDO_PARAMS)
        p['fromDateYear'] = p['toDateYear'] = str(tgl.year)
        p['fromDateMonth'] = p['toDateMonth'] = str(tgl.month)
        p['fromDateDay'] = p['toDateDay'] = str(tgl.year)
        content = self.get_content('/corp/front/balanceinquiry.do', GET_data=p)
        jrprint = get_jrprint(content)
        self.br.select_form(nr=2)
        self.br.set_all_readonly(False)
        self.br['jrprint'] = jrprint
        self.br['index'] = '1'
        self.br['zoom'] = '1'
        url = self.get_url('/reporting/html')
        self.br.form.action = url 
        self.info('POST %s' % url)
        return self.br.submit()


class MutasiBrowser(Browser):
    def __init__(self, username, password, group, output_file=None):
        Browser.__init__(self, username, password, group, MutasiParser,
                         output_file)

    def browse(self, tgl):
        day = str(tgl.day)
        month = str(tgl.month)
        year = str(tgl.year)
        q = {'action': 'menuRequest'}
        self.open_url('/corp/common/login.do', GET_data=q) 
        q = {'action': 'transactionByDateRequest',
             'menuCode': 'MNU_GCME_040200'}
        self.open_url('/corp/front/transactioninquiry.do', GET_data=q)
        self.br.select_form(nr=0)
        self.br.set_all_readonly(False)
        self.br['transferDateDay1'] = self.br['transferDateDay2'] = day
        self.br['transferDateMonth1'] = self.br['transferDateMonth2'] = month
        self.br['transferDateYear1'] = self.br['transferDateYear2'] = year
        self.br['transactionType'] = ['%']
        self.br['accountType'] = ['M']
        self.br['customFile'] = ['Excel']
        self.br['processAccountIndividually'] = ['Y']
        self.br['screenState'] = 'TRX_DATE'
        self.br['permataFlag'] = 'Y'
        i = -1
        for control in self.br.form.controls:
            i += 1
            if control.name:
                if control.name == 'download1':
                    control._value = 'Download'
                if control.name == 'accountHierarchy':
                    control._value = ' '
                elif control.name == 'archiveFlag':
                    control._value = 'Y'
        p = dict(DOWNLOAD_PARAMS)
        p.update({'day1': day, 'mon1': month, 'year1': year,
                  'day2': day, 'mon2': month, 'year2': year})
        url = self.get_url('/corp/front/transactioninquiry.do', p)
        self.br.form.action = url
        self.info('POST %s' % url)
        return self.br.submit()


if __name__ == '__main__':
    import sys
    from optparse import OptionParser
    from pprint import pprint
    from common import (
        to_date,
        open_file,
        )
    pars = OptionParser()
    pars.add_option('-u', '--username')
    pars.add_option('-p', '--password')
    pars.add_option('-g', '--group')
    pars.add_option('-d', '--date', help='dd-mm-yyyy')
    pars.add_option('', '--saldo-file')
    pars.add_option('', '--mutasi-file', help='Zip file')
    pars.add_option('', '--output-file')
    option, remain = pars.parse_args(sys.argv[1:])

    if option.saldo_file:
        content = open_file(option.saldo_file)
        parser = SaldoParser()
        parser.feed(content)
        pprint(parser.get_clean_data())
        sys.exit()

    if option.mutasi_file:
        f = open(option.mutasi_file, 'rb')
        content = f.read()
        f.close()
        parser = MutasiParser()
        parser.feed(content)
        pprint(parser.get_clean_data())
        sys.exit()

    if not option.username or not option.password or not option.group:
        print('--username, --password, dan --group harus diisi')
        sys.exit()

    if option.date:
        crawler = MutasiBrowser(option.username, option.password,
                                option.group, option.output_file)
        tgl = to_date(option.date)
        data = crawler.run(tgl)
        pprint(data)
    else:
        crawler = SaldoBrowser(option.username, option.password,
                               option.group, option.output_file)
        data = crawler.run()
        pprint(data)
